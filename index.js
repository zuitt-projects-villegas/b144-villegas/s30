const express = require("express");

const mongoose = require("mongoose")

const app = express();
const port = 3001

mongoose.connect("mongodb+srv://yaniiix21:clark123@wdc028-course-booking.y3iug.mongodb.net/batch144-to-do?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true
})

// mongoose
let db = mongoose.connection;

db.on("error",console.error.bind(console, "connection error"))
db.once("open", () => console.log("we're connected to the cloud database"))


app.use(express.json())
app.use(express.urlencoded({extended:true}))


const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
})


const Task = mongoose.model("Task", taskSchema)

// Routes

app.post("/tasks", (req, res) => {
	Task.findOne({name: req.body.name}, (error, result) => {
		if(result !== null && result.name == req.body.name){
			return res.send("Duplicate task found")
		}else{
			let newTask = new Task ({
				name: req.body.name
			})

			newTask.save((saveErr, savedTask) => {
				if(saveErr){
					return console.error(saveErr)
				}else{
					return res.status(201).send("New Task Created")
				}
			})
		}
	})
})


// ACTIVITY S30-------------------------------------

app.get("/tasks", (req, res) => {
	Task.find({}, (err, result) => {
		if(err){
			return console.log(err)
		}else{
			return res.status(200).json({
				data: result
			})
		}
	})
})

const userSchema = new mongoose.Schema({
	username: String,
	password: String
})

const User = mongoose.model("User", userSchema)


app.post("/users", (req, res) => {
	User.findOne({username: req.body.username, password: req.body.password}, (error,result) => {
		if (result !== null && result.username == req.body.username){
			return res.send("Duplicate user found")
		}else if(req.body.username == ""){
			return res.send("Username must be provided")			
		}
		else if(req.body.password == ""){
			return res.send("Password must be provided")			
		}  
		else{
			let newUser = new User ({
				username: req.body.username,
				password: req.body.password
			})

			newUser.save((saveErr, savedTask) => {
				if(saveErr){
					return console.error(saveErr)
				}else{
					return res.status(201).send("New User Registered")
				}
			})
		}
	})
})

app.get("/users", (req, res) => {
	User.find({}, (err, result) => {
		if(err){
			return console.log(err)
		}else{
			return res.status(200).json({
				data: result
			})
		}
	})
})

app.listen(port, () => console.log(`Server running at port ${port}`))